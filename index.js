function countLetter(letter, sentence) {
    let result = 0;
    return letter.toLowerCase() != letter.toUpperCase();
    for(let i = 0; i < sentence.length; i++){
        if(sentence[i] == letter){
            result += 1;
            return result;
        }
        else{
            return undefined;
        }
    }
    
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    text = text.toLowerCase();
           let length = text.length;
     
           let arr = text.split('');
     
           arr.sort();
           for (let i = 0; i < length - 1; i++) {
               if (arr[i] == arr[i + 1])
                   return false;
           }
           return true;
               
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    
}

function purchase(age, price) {
    let discountedPrice = price * 0.8
    let roundedPrice = discountedPrice.toFixed(2);
    if(age < 13){
        return undefined
    }
    else if(age >= 13 && age <= 21){
        return roundedPrice
    }
    else if(age >= 22 && age <= 64){
        return roundedPrice
    }
    else{
        return roundedPrice
    }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    let filteredArray = items.filter(eachItem => eachItem.stocks === 0);
    return filteredArray
    let category1 = filteredArray[0].category
    let category2 = filteredArray[1].category
    let category3 = filteredArray[2].category
    return (category1, category2, category3);
    
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = candidateA.filter(element => candidateB.includes(element));
    return flyingVoters;
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};